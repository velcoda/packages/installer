<?php

namespace Velcoda\Installer\Console;

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class NewCommand extends Command
{
    private $directory;

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('new')
            ->setDescription('Create a new Velcoda application')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addOption('project', 'p', InputOption::VALUE_REQUIRED, 'The project this is part of. Used for gitlab-ci.yml file definition and composer file.', 'telebutler')
            ->addOption('branch', null, InputOption::VALUE_REQUIRED, 'The branch that should be created for a new repository', $this->defaultBranch())
            ->addOption('git', null, InputOption::VALUE_NONE, 'Initialize a Git repository')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Forces install even if the directory already exists');
    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commands = [];
        $output->write(PHP_EOL.'  <fg=red>Velcoda Serivce Generator</>'.PHP_EOL.PHP_EOL);

        sleep(1);

        $name = $input->getArgument('name');
        $project = $input->getOption('project');

        $directory = $name !== '.' ? getcwd().'/'.$name : '.';
        $this->directory = $directory;

        if (! $input->getOption('force')) {
            $this->verifyApplicationDoesntExist($directory);
        }

        if ($input->getOption('force') && $directory === '.') {
            throw new RuntimeException('Cannot use --force option when using current directory for installation!');
        }

        if ($directory != '.' && $input->getOption('force')) {
            if (PHP_OS_FAMILY == 'Windows') {
                array_unshift($commands, "(if exist \"$directory\" rd /s /q \"$directory\")");
            } else {
                array_unshift($commands, "rm -rf \"$directory\"");
            }
        }
        $commands[] = "laravel new \"$name\"";
        if (PHP_OS_FAMILY != 'Windows') {
            $commands[] = "chmod 755 \"$directory/artisan\"";
        }
        $exceDir = __DIR__;

        $commands[] = "cd " . $directory;
        $commands[] = $this->findComposer() . " require inspector-apm/inspector-laravel velcoda/exceptions velcoda/api-auth velcoda/transaction-flow velcoda/helpers velcoda/commands velcoda/services -W";
        $commands[] = "cd " . $exceDir;
        $commands[] = $this->replaceFileCmd("app/Console/Kernel.php");
        $commands[] = $this->replaceFileCmd("app/Exceptions/Handler.php");
        $commands[] = $this->replaceFileCmd("app/Http/Kernel.php");
        $commands[] = $this->replaceFileCmd("app/Http/Controllers/HealthzController.php");
        $commands[] = $this->replaceFileCmd("app/Providers/AuthServiceProvider.php");
        $commands[] = $this->replaceFileCmd("app/Providers/RouteServiceProvider.php");
        $commands[] = $this->replaceFileCmd("config/app.php");
        $commands[] = $this->replaceFileCmd("config/auth.php");
        $commands[] = $this->replaceFileCmd("config/database.php");
        $commands[] = $this->replaceFileCmd("routes/healthz.php");
        $commands[] = $this->replaceFileCmd("routes/v1.php");

        $commands[] = $this->replaceFileCmd(".dockerfiles", '-R');
        $commands[] = $this->replaceFileCmd("Dockerfile");
        $commands[] = $this->replaceFileCmd(".env");
        $commands[] = $this->replaceFileCmd(".gitlab-ci.yml");

        $commands[] = $this->deleteFile("app/Providers/BroadcastServiceProvider.php");
        $commands[] = $this->deleteFile("config/broadcasting.php");
        $commands[] = $this->deleteFile("routes/channels.php");
        $commands[] = $this->deleteFile("routes/console.php");
        $commands[] = $this->deleteFile("routes/web.php");
        $commands[] = $this->deleteFile("routes/api.php");
        $commands[] = $this->deleteFile("app/Http/Middleware/*.php");
        $commands[] = $this->deleteFile("vite.config.js");
        $commands[] = $this->deleteFile("README.md");
        $commands[] = $this->deleteFile("package.json");

        // uninstall sanctum
        $commands[] = $this->deleteFile("app/Models/User.php");
        $commands[] = $this->deleteFile("config/sanctum.php");
        $commands[] = "cd " . $directory;
        $commands[] = $this->findComposer() . " remove laravel/sanctum";
        $commands[] = PHP_BINARY . ' artisan key:generate';
        $commands[] = PHP_BINARY . ' artisan access-key';

        if (($process = $this->runCommands($commands, $input, $output))->isSuccessful()) {
            if ($name !== '.') {

                $this->replaceInFile('servicename', $name, $directory.'/app/Providers/RouteServiceProvider.php');
                $this->replaceInFile('servicename', $name, $directory.'/config/app.php');
                $this->replaceInFile('servicename', $name, $directory.'/.env');
                $this->replaceInFile('servicename', $name, $directory.'/.env.example');
                $this->replaceInFile('servicename', $name, $directory.'/.gitlab-ci.yml');
                $this->replaceInFile('project', $project, $directory.'/.gitlab-ci.yml');
                $this->replaceInFile('laravel/laravel', $project.'/'.$name, $directory.'/composer.json');
                $this->replaceInFile('The Laravel Framework.', 'Backend Service: ' . $name, $directory.'/composer.json');
                $this->replaceInFile('["framework", "laravel"]', '["' . $name . '", "velcoda"]', $directory.'/composer.json');
            }

            $this->createRepository($directory, $input, $output);

            $output->writeln('  <bg=blue;fg=white> INFO </> Application ready! <options=bold>Build something amazing.</>'.PHP_EOL);
        }

        return $process->getExitCode();
    }

    protected function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Create a Git repository and commit the base Laravel skeleton.
     *
     * @param  string  $directory
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    protected function createRepository(string $directory, InputInterface $input, OutputInterface $output)
    {
        chdir($directory);

        $branch = $input->getOption('branch') ?: $this->defaultBranch();
        $project = $input->getOption('project');
        $commands = [
            'git init -q',
            'git add .',
            'git commit -q -m "Set up a fresh ' . $project . ' application"',
            "git branch -M {$branch}",
        ];

        $this->runCommands($commands, $input, $output);
    }


    /**
     * Return the local machine's default Git branch if set or default to `main`.
     *
     * @return string
     */
    protected function defaultBranch()
    {
        $process = new Process(['git', 'config', '--global', 'init.defaultBranch']);

        $process->run();

        $output = trim($process->getOutput());

        return $process->isSuccessful() && $output ? $output : 'main';
    }

    /**
     * Commit any changes in the current working directory.
     *
     * @param  string  $message
     * @param  string  $directory
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    protected function commitChanges(string $message, string $directory, InputInterface $input, OutputInterface $output)
    {
        chdir($directory);

        $commands = [
            'git add .',
            "git commit -q -m \"$message\"",
        ];

        $this->runCommands($commands, $input, $output);
    }


    /**
     * Verify that the application does not already exist.
     *
     * @param  string  $directory
     * @return void
     */
    protected function verifyApplicationDoesntExist($directory)
    {
        if ((is_dir($directory) || is_file($directory)) && $directory != getcwd()) {
            throw new RuntimeException('Application already exists!');
        }
    }

    /**
     * Generate a valid APP_URL for the given application name.
     *
     * @param  string  $name
     * @return string
     */
    protected function generateAppUrl($name)
    {
        $hostname = mb_strtolower($name).'.test';

        return $this->canResolveHostname($hostname) ? 'http://'.$hostname : 'http://localhost';
    }

    /**
     * Determine whether the given hostname is resolvable.
     *
     * @param  string  $hostname
     * @return bool
     */
    protected function canResolveHostname($hostname)
    {
        return gethostbyname($hostname.'.') !== $hostname.'.';
    }

    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer()
    {
        $composerPath = getcwd().'/composer.phar';

        if (file_exists($composerPath)) {
            return '"'.PHP_BINARY.'" '.$composerPath;
        }

        return 'composer';
    }

    /**
     * Run the given commands.
     *
     * @param  array  $commands
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @param  array  $env
     * @return \Symfony\Component\Process\Process
     */
    protected function runCommands($commands, InputInterface $input, OutputInterface $output, array $env = [])
    {
        if (! $output->isDecorated()) {
            $commands = array_map(function ($value) {
                if (substr($value, 0, 5) === 'chmod') {
                    return $value;
                }

                return $value.' --no-ansi';
            }, $commands);
        }

        if ($input->getOption('quiet')) {
            $commands = array_map(function ($value) {
                if (substr($value, 0, 5) === 'chmod') {
                    return $value;
                }

                return $value.' --quiet';
            }, $commands);
        }

        $process = Process::fromShellCommandline(implode(' && ', $commands), null, $env, null, null);

        if ('\\' !== DIRECTORY_SEPARATOR && file_exists('/dev/tty') && is_readable('/dev/tty')) {
            try {
                $process->setTty(true);
            } catch (RuntimeException $e) {
                $output->writeln('  <bg=yellow;fg=black> WARN </> '.$e->getMessage().PHP_EOL);
            }
        }

        $process->run(function ($type, $line) use ($output) {
            $output->write('    '.$line);
        });

        return $process;
    }

    /**
     * Replace the given string in the given file.
     *
     * @param  string  $search
     * @param  string  $replace
     * @param  string  $file
     * @return void
     */
    protected function replaceInFile(string $search, string $replace, string $file)
    {
        file_put_contents(
            $file,
            str_replace($search, $replace, file_get_contents($file))
        );
    }

    /**
     * Replace the original with an override
     *
     */
    protected function replaceFileCmd(string $file, string $options = ""): string
    {   
        return "cp " . $options . " " . __DIR__ . "/overrides/" . $file . " " . $this->directory . "/" . $file;    
    }

    /**
     * Replace the original with an override
     *
     */
    protected function deleteFile(string $file, string $options = ""): string
    {
        return "rm " . $options . " " . $this->directory . "/" . $file;
    }
}
