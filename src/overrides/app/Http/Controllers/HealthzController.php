<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HealthzController extends Controller
{
    public function readiness(Request $request)
    {
        // Test MYSQL database connection
        DB::connection()->getPdo();
        return 'ok';
    }

    public function liveness(Request $request)
    {
        return 'ok';
    }

    public function startup(Request $request)
    {
        // for the sake of having all three k8s probes
        // In here should be all necessary implementations that need to
        // be done in the startup procedure before the pod can start getting requests
        return 'ok';
    }
}
