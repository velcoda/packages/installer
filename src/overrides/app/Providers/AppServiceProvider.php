<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Velcoda\Helpers\NewRelic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $env = env('APP_ENV');
        $project = env('APP_PROJECT', 'velcoda');
        $name = env('APP_NAME');
        $service_name = strtoupper($env) . ' – ' . strtoupper($project) . ' – '. $name;

        NewRelic::setName();
        Inspector::beforeFlush(function ($inspector) use ($service_name) {
            $inspector->currentTransaction()
                ->host
                ->hostname = $service_name;
        });
    }
}
