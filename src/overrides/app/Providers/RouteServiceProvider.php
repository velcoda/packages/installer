<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group(['prefix' => 'servicename'], function () {
            Route::prefix('healthz')
                ->namespace($this->namespace)
                ->middleware('throttle:velcoda-healthz')
                ->group(base_path('routes/healthz.php'));

            Route::prefix('v1')
                ->middleware('api')
                ->middleware('throttle:velcoda')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1.php'));
        });
    }
}
