<?php

use App\Http\Controllers\HealthzController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/startup', [HealthzController::class, 'startup']);
Route::get('/readiness', [HealthzController::class, 'readiness']);
Route::get('/liveness', [HealthzController::class, 'liveness']);
